# ---
# jupyter:
#   jupytext:
#     formats: ipynb,py:percent
#     text_representation:
#       extension: .py
#       format_name: percent
#       format_version: '1.3'
#       jupytext_version: 1.14.1
#   kernelspec:
#     display_name: AiiDA
#     language: python
#     name: aiida
# ---

# %% [markdown] tags=[] jp-MarkdownHeadingCollapsed=true
# # Pandas How-tos
#
# First point to check is always the [Pandas Getting Started tutorials](https://pandas.pydata.org/docs/getting_started/index.html#intro-to-pandas).

# %% [markdown] tags=[] jp-MarkdownHeadingCollapsed=true
# ## Data access, indexing
#
# Indexing: loc, iloc, at, iat for accessing parts of the dataframe. loc and at are for value-based indexing (eg, column name, DataFrame `Index` = row name). iloc and iat are for numeric indexing (like in an array, supply integer n to get the n-th element).
#
# Summary.
#
# - **Note that the Index (=row index) can also be a list of labels. `loc` and `at` are for this kind of index.** If the index is just the default numerical one, `loc`/`iloc`, `at`/`iat` behave the same way towards it.
# - **Note that all these selectors can be used as getters *and* setters.**
# - Note that the indexer `df.ix[...]` is deprecated.
# - `at[row_idx, col_idx]` accesses a single cell (row+column), `iat[num_row_idx, num_col_idx]` does, too, with numerical indices.
# - `loc[]` selects rows. (In the following, `idx` stands for value-based index for `loc`, numeric index for `iloc`).
#   - `i/loc[row_idx]` selects that row
#   - `i/loc[row_indices]` selects several rows, `row_indices` is a list of indices, e.g. `.iloc[2,-2]` selects second and penultimate row.
#   - `i/loc[row_idx1:row_idx2]` (slice notation) selects a range of rows
#   - `i/loc[row_indices, col_idx:]` selects list of rows and columns from `col_idx` to end
#   - `df.loc[row_indices, df.columns[[2,4]]` selects rows and 2nd and 4th colunn by column name
#   - `df.loc[df['age'] > 30, ['food', 'score']]` and `df.iloc[(df['age'] > 30).to_numpy(), [2, 4]]` select both the same thing: all the rows where age is above 30, with the coluns 2,4. for `iloc`, the booleans have to converted to a numpy array
#   - `df.loc[((df['a'] > 1) & (df['b'] > 0)) | ((df['a'] < 1) & (df['c'] == 100))]` select all rows which satisfies these multiple conditions. Note boolean condition syntax: `&` for logical and, `|` for logical or.
#   - `df.loc[:, 'color':'score':2]` selects all the rows, with every other column between `color` and `score`
#
# References.
#
# - [pandas > Indexing and selecting data](https://pandas.pydata.org/pandas-docs/stable/user_guide/indexing.html)
# - [use at/iat to get/set a cell value](https://stackoverflow.com/a/13842286/8116031)
# - [detailed explanation of loc/iloc, at/iat with examples](https://stackoverflow.com/a/47098873/8116031)
#
#   

# %% [markdown] tags=[] jp-MarkdownHeadingCollapsed=true
# ## Iterating over DataFrames  
#
# Note on pandas performance wrt iteration over dataframe: there are [various ways to iterate of a DataFrame](https://pandas.pydata.org/pandas-docs/stable/user_guide/basics.html#iteration), and [this SO thread discusses the most efficient ways](https://stackoverflow.com/questions/16476924/how-to-iterate-over-rows-in-a-dataframe-in-pandas):
# - [how to iterate over a dataframe? DON'T!](https://stackoverflow.com/a/55557758/8116031)
# - [how to iterate over a dataframe efficiently, if you have to](https://stackoverflow.com/a/59413206/8116031)
# In summary: vectorize if you can, use list comprehensions if you can. else if you must use itertuples, but not iterrows as it doesn't preserve datatype and is slower. the speed differences between each method are orders of magnitude. but mind though that they are often talking about dataframes with millions of rows. and i have ten thousand.
#
# Using `itertuples`:
# ```python
# for row in df.itertuples():
#     # row[0] is the index, the rest of the tuple are the row values
#     print(row)
# ```
#
# Using list comprehensions:
# ```python
# # Iterating over one column - `f` is some function that processes your data
# result = [f(x) for x in df['col']]
# # Iterating over two columns, use `zip`
# result = [f(x, y) for x, y in zip(df['col1'], df['col2'])]
# # Iterating over multiple columns - same data type
# result = [f(row[0], ..., row[n]) for row in df[['col1', ...,'coln']].to_numpy()]
# # Iterating over multiple columns - differing data type
# result = [f(row[0], ..., row[n]) for row in zip(df['col1'], ..., df['coln'])]
# # added by JW: pass row index along as well
# [f(i,a,b) for i,a,b in zip(df.index, df['col1'], df['col2'])]
# ```
#
# so my list comprehension example above shows that i can use list comprehension as a complete replacement for `itertuples`, with the speed benefit of nearly reaching vectorized performance.
#
# An applied example of efficiently iterating over a dataframe and manipulating it at the same time, using a list comprehension:
#
# ```python
# bla = pd.DataFrame(data={'a' : [5,6,7], 'b':[None,None,None]})
# def foo(i,a):
#     bla.at[i,'b'] = a*2
# [foo(i,a) for i,a in zip(bla.index, bla['a'])]
# ```

# %% [markdown]
# ### Sorting data
#
# sorting:
# - [pandas > pandas.DataFrame.sort_values()](https://pandas.pydata.org/pandas-docs/stable/reference/api/pandas.DataFrame.sort_values.html): also sort by multiple columns. Note: `ascending` can also be a list, just like `by`.
